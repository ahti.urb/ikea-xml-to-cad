/**
 * GLOBALS
 */
 var catalogueArray = [[]] //container array for product catalogue


 /* 
 https://stackoverflow.com/questions/1293147/example-javascript-code-to-parse-csv-data
 ref: http://stackoverflow.com/a/1293163/2343
 --
 This will parse a delimited string into an array of
 arrays. The default delimiter is the comma, but this
 can be overriden in the second argument.
*/
function CSVToArray( strData, strDelimiter=',' ){
  // Check to see if the delimiter is defined. If not,
  // then default to comma.
  strDelimiter = (strDelimiter || ",");

  // Create a regular expression to parse the CSV values.
  var objPattern = new RegExp(
      (
          // Delimiters.
          "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

          // Quoted fields.
          "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

          // Standard fields.
          "([^\"\\" + strDelimiter + "\\r\\n]*))"
      ),
      "gi"
      );

  // Create an array to hold our data. Give the array
  // a default empty first row.
  var arrData = [[]];

  // Create an array to hold our individual pattern
  // matching groups.
  var arrMatches = null;

  // Keep looping over the regular expression matches
  // until we can no longer find a match.
  while (arrMatches = objPattern.exec( strData )){

      // Get the delimiter that was found.
      var strMatchedDelimiter = arrMatches[ 1 ];

      // Check to see if the given delimiter has a length
      // (is not the start of string) and if it matches
      // field delimiter. If id does not, then we know
      // that this delimiter is a row delimiter.
      if (
          strMatchedDelimiter.length &&
          strMatchedDelimiter !== strDelimiter
          ){

          // Since we have reached a new row of data,
          // add an empty row to our data array.
          arrData.push( [] );
      }

      var strMatchedValue;

      // Now that we have our delimiter out of the way,
      // let's check to see which kind of value we
      // captured (quoted or unquoted).
      if (arrMatches[ 2 ]){
          // We found a quoted value. When we capture
          // this value, unescape any double quotes.
          strMatchedValue = arrMatches[ 2 ].replace(
              new RegExp( "\"\"", "g" ),
              "\""
              );
      } else {
          // We found a non-quoted value.
          strMatchedValue = arrMatches[ 3 ];
      }

      // Now that we have our value string, let's add
      // it to the data array.
      arrData[ arrData.length - 1 ].push( strMatchedValue );
  }

  // Return the parsed data.
  return( arrData );
}



/**
 * 
 */
function loadCatalogueData (catalogue_csv_data) {
  console.log('loadCatalogueData: starting ...')
  //note: catalogueArray defined in GLOBALS
  catalogueArray = CSVToArray(catalogue_csv_data)
  console.log('loadCatalogueData: done.')
}

/**
 * 
 */
function loadCatalogueCSVFile(ev) {
    var file = ev.target.files[0]
    if (!file) {
      return
    }
    var catReader = new FileReader()
    catReader.onload = function(ev) { 
      loadCatalogueData(ev.target.result)
    }
    catReader.readAsText(file, 'UTF-8')
}


/**
 * Looks for an exact match in column 0 in given CSV file 
 * for a product catalogue code and returns a Product Description 
 * from CSV column 1.
 * @param {String} in_lookup_str 
 */
 function catalogueLookup(in_lookup_str) {
  let cat_code = ''
  if (isNaN(in_lookup_str)) {
    //cut prefix string part
    cat_code = String(in_lookup_str).substring(4)
  } else cat_code = in_lookup_str

  if (!cat_code) return ""
  
  console.log('catalogueLookup: matching "' + cat_code + '"')

  //look up description based on article code
  //note: catalogueArray defined in GLOBALS
  for (const cat_element of catalogueArray) {
    if (cat_code == String(cat_element[0]).trim()) {
      console.log('catalogueLookup: found in col0 ' + cat_element )
      //return description for the matching atricle code
      return String(cat_element[1])
    } else if (cat_code == String(cat_element[1]).trim()) {
      console.log('catalogueLookup: found in col1 ' + cat_element )
      //return description for the matching atricle code
      return String(cat_element[4])
    } else if (cat_code == String(cat_element[2]).trim()) {
      console.log('catalogueLookup: found in col2 ' + cat_element )
      //return description for the matching atricle code
      return String(cat_element[4])
    } else if (cat_code == String(cat_element[3]).trim()) {
      console.log('catalogueLookup: found in col3 ' + cat_element )
      //return description for the matching atricle code
      return String(cat_element[4])
    }
  }//for

  //by default return this if not match found
  return "N/A"
} //catalogueLookup

/**
 * Reads a Text file and passes the contents to function <parseFunctionName>
 * @param {String} e
 * @returns None
 */
function readAndParseSingleFile(ev, parseFunctionName) {
    var file = ev.target.files[0]
    if (!file) {
      return
    }
    var reader = new FileReader()
    var content = '8'
    reader.onload = function(ev) {
        content = ev.target.result
        var fn = window[parseFunctionName]
        if (typeof fn === 'function') {
            fn(content)
        } 
        console.log('readAndParseSingleFile: done.')
    }
    reader.readAsText(file, 'UTF-8')
  }
  
/**
 * Render given element in HTML page with given text/HTML content
 * @param {String} content
 * @param {String} el 
 * @param {Boolean} append 
 */
function displayContents(content, el="file-content", append=false) {
    let doc_element = document.getElementById(el);
    if (!doc_element) {
      console.log('displayContents: element "'+el+'" not found!')
    }
    if (append) doc_element.innerHTML =  doc_element.innerHTML + content;
    else doc_element.innerHTML = content;
  } 

  function escapeHTML(in_str) {
    return in_str
      .replaceAll("\t","&#9;")
      .replaceAll(" ", "&nbsp;")
      .replaceAll("\n","</br>")
  }
  
  /**
   * Generates custom output based on key name
   * @param {JSON.JSON} in_json 
   * @returns HTML string
   */
  function generateHTML(in_prefix, in_key, in_value) {
    out_html = '<div class="top">'
    if (in_key.toLowerCase().includes("description")) {
      //display value in a textarea for any key name that matches "description"
      out_html += '<span class="top">' 
        + escapeHTML(in_prefix)
        + in_key + ': </span><textarea rows="1" cols="60">' + escapeHTML(in_value)
        + "</textarea>" 
    } else {
      //default
      out_html += escapeHTML(in_prefix) + 
        in_key + ": " + 
        escapeHTML(in_value)
    }
    out_html += "</div>\n"
    return out_html
  }
  
  /**
   * 
   * @param {JSON.JSON} in_obj 
   * @param {int} in_level 
   */
  function printAllVals(in_obj,in_level=0) {
    let out_html = ""
    let prefix = ""
  
    for (let step = 0; step <= in_level; step++) {
      prefix += "-"
    }
    prefix += " "
    
    for (let k in in_obj) {
      let out_str = ""
      if (typeof in_obj[k] === "object") { //if node has children (is a container)
        out_str += prefix + k + "\n"
        out_html += generateHTML(prefix, k, "")
        let next_level = in_level+1
        out_html += printAllVals(in_obj[k],level=next_level)
      } else { //if it's a leaf node containing a value
        out_html += generateHTML(prefix, k, in_obj[k])
      }
    }
    return out_html
  }

  
/**
 * Removes useless nodes from Document Object Model (DOM)
 * Credits to James Edwards https://www.sitepoint.com/removing-useless-nodes-from-the-dom/
 * @param {DOM Node Object} node 
 */
function cleanDom(node)
{
  for(var n = 0; n < node.childNodes.length; n ++)
  {
    var child = node.childNodes[n];
    if
    (
      child.nodeType === 8 
      || 
      (child.nodeType === 3 && !/\S/.test(child.nodeValue))
    )
    {
      node.removeChild(child);
      n --;
    }
    else if(child.nodeType === 1)
    {
      cleanDom(child);
    }
  }
}

/**
 * Parses points with X and Y coordinates from given DOM element and returns
 * a Hashmap object
 * @param {DOM Node} in_element 
 * @returns Hashmap  
 */
function parsePoints(in_element) {
  let points = {}
  
  in_element.getElementsByTagName("Points")[0].childNodes.forEach(point => {
    let attr = {}
    attr["x"] = point.attributes.x.textContent
    attr["y"] = point.attributes.y.textContent
    points[point.attributes.id.textContent] = attr
  })
  return points
}

function parseEdges(in_element) {
  let edges = []
  
  in_element.getElementsByTagName("StraightEdge")[0].childNodes.forEach(edge => {
    let this_edge = {}
    try {
      this_edge['EdgeDesign'] = edge.getElementsByTagName("EdgeDesign")[0].textContent
      this_edge['ProductNo'] = edge.getElementsByTagName("ProductNo")[0].textContent
      this_edge['ProductDescription'] = catalogueLookup(this_edge['ProductNo'])
      edges.push(this_edge)
    } catch (error) {
      //console.log(error)
    }
  })
  return edges
}

function parseCutOuts(shape) {
  let cutouts = {}
  shape.childNodes.forEach(shape_item => {
    if (shape_item.textContent.length > 1 
      && shape_item.getElementsByTagName("CutOut") 
      && shape_item.getElementsByTagName("Radius").length) { //look only round shaped cutouts (with radius defined)
      let position = shape_item.getElementsByTagName("Position")
      let radius = shape_item.getElementsByTagName("Radius")
      let size = shape_item.getElementsByTagName("Size")
      cutouts[shape_item.attributes.id.textContent] = {}
      cutouts[shape_item.attributes.id.textContent]["position"] = position[0].firstChild.id
      cutouts[shape_item.attributes.id.textContent]["radius"] = radius[0].textContent
      if (size.length) {
        cutouts[shape_item.attributes.id.textContent]["size"] = {}
        cutouts[shape_item.attributes.id.textContent]["size"]["height"] = size[0].attributes.height.textContent
        cutouts[shape_item.attributes.id.textContent]["size"]["width"] = size[0].attributes.width.textContent
      }
      cutouts[shape_item.attributes.id.textContent]["RefProduct"] = shape_item.getElementsByTagName("RefProduct")[0].textContent
      cutouts[shape_item.attributes.id.textContent]["RefProductDescription"] = catalogueLookup(cutouts[shape_item.attributes.id.textContent]["RefProduct"])
    }  
  })
  return cutouts
}

/**
 * Main execution controller function for Worktops
 * @param {*} xml_content 
 */
function parseWorkTops(xml_content) {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml_content,"text/xml");
    cleanDom(xmlDoc)

    let wt_image_url = xmlDoc.getElementsByTagName("WorktopImageURL")
    if (wt_image_url) {
      img_link = '<a href="' + wt_image_url[0].textContent + '" target="_new">IKEA WorktopImageURL</a>'
      displayContents(img_link, 'worktop-image-uri', true)
      //open link automatically in new browser tab
      //window.open(wt_image_url[0].textContent,'ikea_url')
    }
    

    var worktops = []
    items = xmlDoc.getElementsByTagName("Items")
    
    //Loop through Worktops
    items[0].childNodes.forEach(element => {
        if (element.nodeName != 'Worktop') return; //only process "Worktop" items, skip the rest
        let worktop = {}
        worktop['name'] = element.nodeName
        worktop['id'] = element.attributes.id.textContent
        worktop['ProductNo'] = element.getElementsByTagName("ProductNo")[0].textContent
        worktop['ProductDescription'] = catalogueLookup(worktop['ProductNo'])
        //Points
        if (element.textContent.length > 1 && element.getElementsByTagName("Points")) { 
          worktop["Points"] = parsePoints(element)
        }
        //CutOuts
        if (element.textContent.length > 1 && element.getElementsByTagName("Shape")) {
          worktop["CutOuts"] = parseCutOuts(element.getElementsByTagName("Shape")[0])
          worktop["Edges"] = parseEdges(element.getElementsByTagName("Shape")[0])
        }
        worktops.push(worktop)
    })

    let content = ""
    //console.log(worktops)
    content = printAllVals(worktops)

    //displayContents("<pre>" + JSON.stringify(worktops, null, 2) + "</pre>", 'file-content')
    displayContents(content, 'file-content')
    
    let cad_cmd = ""

    worktops.forEach(wt => {
      //kontuur
      cad_cmd += "_polyline\n"
      for (let i=1; i<=Object.keys(wt['Points']).length+1; i++) { 
        cad_cmd += wt['Points'][i]['x'] + "," + wt['Points'][i]['y'] + "\n"
        if (i > 1 && parseInt(wt['Points'][i]['y']) == 0) {
          break
        } //end of shape, close of contour
      }
      cad_cmd += "C\n"

      if (wt['CutOuts']) {
        for (const [co_key, co] of Object.entries(wt['CutOuts'])) {
          let cutout_position = co['position']
          if (co['radius'] && !co['size']) {
            circle_x = parseFloat(wt['Points'][cutout_position]['x']) 
            circle_y = parseFloat(wt['Points'][cutout_position]['y'])
            cad_cmd += "_circle\n"
            cad_cmd += circle_x + "," + circle_y + "\n"
            cad_cmd += "R\n"
            cad_cmd += co['radius'] + "\n"
          }
          if (co['size']) { //rectangle
            cad_cmd += "_rectangle\n"
            cad_cmd += "fillet\n"
            cad_cmd += co['radius'] + "\n"
    
            rect_x = parseFloat(wt['Points'][cutout_position]['x']) - (parseFloat(co['size']['width']) / 2)
            rect_y = parseFloat(wt['Points'][cutout_position]['y']) - (parseFloat(co['size']['height']) / 2)
            cad_cmd += rect_x + "," + rect_y + "\n"
            cad_cmd += "D\n"
            cad_cmd += co['size']['width'] + "\n"
            cad_cmd += co['size']['height'] + "\n"
            cad_cmd += rect_x + "," + rect_y + "\n"
          } //if
        } //end for CutOuts
      }//end if CutOuts
      
      cad_cmd += "_ai_selall\n"
      cad_cmd += "_move\n"
      cad_cmd += "0,0\n"
      cad_cmd += "0,1100\n"
    }) //end forEach
    
    cad_cmd += "C\n"
    cad_cmd += "_zoom\n"
    cad_cmd += "E\n"
    cad_cmd += "_enter\n"

    displayContents(cad_cmd, 'cad-commands', true)
}

/**
 * Main execution controller function for WallPanels
 * @param {*} xml_content 
 */
 function parseWallPanels(xml_content) {
  parser = new DOMParser();
  xmlDoc = parser.parseFromString(xml_content,"text/xml");
  cleanDom(xmlDoc)

  /*
  let wt_image_url = xmlDoc.getElementsByTagName("WorktopImageURL")
  if (wt_image_url) {
    img_link = '<a href="' + wt_image_url[0].textContent + '" target="_new">IKEA WorktopImageURL</a>'
    displayContents(img_link, 'worktop-image-uri', true)
    //open link automatically in new browser tab
    window.open(wt_image_url[0].textContent,'ikea_url')
  }
  */
  

  var wallpanels = [];
  items = xmlDoc.getElementsByTagName("Items")
  
  //Loop through wallpanels
  items[0].childNodes.forEach(element => {
      if (element.nodeName != 'WallPanel') return; //only process "Worktop" items, skip the rest
      let worktop = {}
      worktop['name'] = element.nodeName
      worktop['id'] = element.attributes.id.textContent
      worktop['ProductNo'] = element.getElementsByTagName("ProductNo")[0].textContent
      worktop['ProductDescription'] = catalogueLookup(worktop['ProductNo'])
      //Points
      if (element.textContent.length > 1 && element.getElementsByTagName("Points")) { 
        worktop["Points"] = parsePoints(element)
      }
      //CutOuts
      if (element.textContent.length > 1 && element.getElementsByTagName("Shape")) {
        worktop["CutOuts"] = parseCutOuts(element.getElementsByTagName("Shape")[0])
        worktop["Edges"] = parseEdges(element.getElementsByTagName("Shape")[0])
      }
      wallpanels.push(worktop)
  });
  

  let content = ""
  //console.log(wallpanels)
  content = printAllVals(wallpanels)

  //displayContents("<pre>" + JSON.stringify(wallpanels, null, 2) + "</pre>", 'file-content')
  displayContents(content, 'file-content')
  
  var cad_cmd = ""

  wallpanels.forEach(wp => {
    //kontuur
    cad_cmd += "_polyline\n"
    for (let i=1; i<=Object.keys(wp['Points']).length+1; i++) { 
      cad_cmd += wp['Points'][i]['x'] + "," + wp['Points'][i]['y'] + "\n"
      if (i > 1 && parseInt(wp['Points'][i]['y']) == 0) {
        break
      } //end of shape, close of contour
    }
    cad_cmd += "C\n"

    if (wp['CutOuts']) {
      for (const [co_key, co] of Object.entries(wp['CutOuts'])) {
        let cutout_position = co['position']
        if (co['radius'] && !co['size']) {
          circle_x = parseFloat(wp['Points'][cutout_position]['x']) 
          circle_y = parseFloat(wp['Points'][cutout_position]['y'])
          cad_cmd += "_circle\n"
          cad_cmd += circle_x + "," + circle_y + "\n"
          cad_cmd += "R\n"
          cad_cmd += co['radius'] + "\n"
        }
        if (co['size']) { //rectangle
          cad_cmd += "_rectangle\n"
          cad_cmd += "fillet\n"
          cad_cmd += co['radius'] + "\n"
  
          rect_x = parseFloat(wp['Points'][cutout_position]['x']) - (parseFloat(co['size']['width']) / 2)
          rect_y = parseFloat(wp['Points'][cutout_position]['y']) - (parseFloat(co['size']['height']) / 2)
          cad_cmd += rect_x + "," + rect_y + "\n"
          cad_cmd += "D\n"
          cad_cmd += co['size']['width'] + "\n"
          cad_cmd += co['size']['height'] + "\n"
          cad_cmd += rect_x + "," + rect_y + "\n"
        } //if
      } //end for CutOuts
    }//end if CutOuts
    
    cad_cmd += "_ai_selall\n"
    cad_cmd += "_move\n"
    cad_cmd += "0,0\n"
    cad_cmd += "0,1100\n"
  }) //end forEach
  
  cad_cmd += "C\n"
  cad_cmd += "_zoom\n"
  cad_cmd += "E\n"
  cad_cmd += "_enter\n"

  displayContents(cad_cmd, 'cad-commands', true)
}
/*
-- moved this to index.html ----
document.getElementById('file-input')
.addEventListener('change', readAndParseSingleFile, false);
*/